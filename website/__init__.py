import os

from flask import Flask, render_template, request, g, Response
from .db import search_products, image_from_product_id

import re


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'website.sqlite')
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    def is_whitespace(s):
        return re.match(r'^\s+$', s)

    @app.route('/', methods=['GET', 'POST'])
    def home_page():
        search_results = None
        g.search_performed = False
        query = request.form.get('query')

        if request.method == 'POST' and query is not None and not is_whitespace(query):
            search_results = search_products(query)
            g.search_performed = True

        return render_template('home-page.html', search_results=search_results)

    @app.route('/get_image')
    def get_image():
        try:
            product_id = int(request.args.get('id'))
            img = image_from_product_id(product_id)

            return Response(
                response=img,
                mimetype='image/jpeg',
                status=200)

        except TypeError:
            return Response(status=404)

    from . import db
    db.init_app(app)

    return app
