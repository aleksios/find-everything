import sqlite3

import click
from flask import current_app, g
from flask.cli import with_appcontext


def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)


@click.command('init-db')
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables."""
    init_db()
    click.echo('Database initialised successfully.')


def init_db():
    db = get_db()

    click.echo('Executing schema.')
    with current_app.open_resource('schema.sql') as f:
        db.executescript(f.read().decode('utf8'))

    click.echo('Populating database.')
    populate_db()


def get_db():
    if 'db' not in g:
        g.db = sqlite3.connect(
            current_app.config['DATABASE'],
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.db.row_factory = sqlite3.Row

    return g.db


def search_products(query):
    db_conn = get_db()
    cursor = db_conn.cursor()

    results = []
    query = query.lower()

    for term in query.split(' '):
        term = '%{}%'.format(term)
        cursor.execute("""
            SELECT p.id, p.title, p.price, p.description, p.details
            FROM product AS p WHERE
            lower(p.title) LIKE ? OR
            lower(p.description) LIKE ? OR
            lower(p.details) LIKE ?
        """, (term, term, term))
        db_conn.commit()

        rows = cursor.fetchall()
        for row in rows:
            results.append(row)

    cursor.close()
    return results


def image_from_product_id(product_id):
    db_conn = get_db()
    cursor = db_conn.cursor()

    cursor.execute('SELECT p.image FROM product AS p WHERE p.id = ?', (product_id,))
    sqlite_row = cursor.fetchone()
    cursor.close()

    return sqlite_row['image']


def populate_db():
    db_conn = get_db()
    cursor = db_conn.cursor()

    products = [
        ('Bananas', 170, blob_from_res('bananas.jpeg')),
        ('Brussels Sprouts', 150, blob_from_res('brussels_sprouts.jpeg')),
        ('Carrots', 99, blob_from_res('carrots.jpeg')),
        ('Garlics', 76, blob_from_res('garlics.jpeg')),
        ('Grapes', 135, blob_from_res('grapes.jpeg')),
        ('Green Apples', 190, blob_from_res('green_apples.jpeg')),
        ('Green Peas', 160, blob_from_res('green_peas.jpeg')),
        ('Honeycrisp Apples', 235, blob_from_res('honeycrisp_apples.jpeg')),
        ('Lemons', 64, blob_from_res('lemons.jpeg')),
        ('Onions', 70, blob_from_res('onions.jpeg')),
        ('Oranges', 150, blob_from_res('oranges.jpeg')),
        ('Savoy Cabbage', 112, blob_from_res('savoy_cabbage.jpeg')),
        ('Tomatoes', 140, blob_from_res('tomatoes.jpeg')),
    ]

    cursor.executemany('INSERT INTO product (title, price, image) VALUES (?, ?, ?)', products)

    cursor.close()
    db_conn.commit()


def blob_from_res(res_name):
    from os import sep
    path = 'res4db' + sep + res_name

    with current_app.open_resource(path, 'rb') as f:
        return sqlite3.Binary(f.read())


def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()
