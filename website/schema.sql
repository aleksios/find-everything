DROP TABLE IF EXISTS product;

CREATE TABLE product (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  image BLOB,
  title TEXT NOT NULL,
  price INTEGER NOT NULL,
  description TEXT,
  details TEXT
);
