# Find Everything

An online shop prototype for a school project.

# Screenshots

## Index page

![Index page](./screenshots/index-page.png "Index page")

---

## All products

![All products](./screenshots/all-products.png "All products")

# Running

## Debian GNU/Linux

1. `python3 -m venv venv`

2. `. ./venv/bin/activate`

3. `pip install -U pip`

4. `pip install -r requirements.txt`

5. `export FLASK_APP=website && flask init-db && flask run`

